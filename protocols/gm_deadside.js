const Core = require("./core");

class Gm_deadside extends Core {
  constructor() {
    super();
    this.usedTcp = true;
  }

  async run(state) {
    await super.run(state);
    let serversRAW = [];
    let servers = [];

    {
      const raw = await this.request({
        url: "http://51.89.1.248/dsapi/serverlist",
      });
      const json = JSON.parse(raw);
      serversRAW = serversRAW.concat(json.serverlist);
    }

    {
      serversRAW.forEach((server) => {
        servers[server.addr] = server;
      });

      const raw = servers[this.options.address + ":" + this.options.port];
      state.raw.numplayers = raw.players;
      state.raw.gameid = 895400;
      state.raw.version = raw.serverversion;
      state.name = raw.id;
      state.password = !!raw.password;
      state.maxplayers = raw.playersmax;
    }
  }
}

module.exports = Gm_deadside;
