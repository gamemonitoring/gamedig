const Valve = require("./valve");

class Kf2 extends Valve {
  constructor() {
    super();
  }
  async run(state) {
    await super.run(state);
    state.players = state.bots;
    state.bots = [];
    state.raw.numplayers = state.raw.numbots;
    state.raw.numbots = state.bots.length;
  }
}

module.exports = Kf2;
