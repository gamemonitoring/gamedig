const Quake2 = require("./quake2");

class Gm_fivem extends Quake2 {
  constructor() {
    super();
    this.sendHeader = "getinfo xxx";
    this.responseHeader = "infoResponse";
    this.encoding = "utf8";
  }

  async run(state) {
    await super.run(state);

    {
      const raw = await this.request({
        url:
          "http://" +
          this.options.address +
          ":" +
          this.options.port +
          "/info.json"
      });
      const json = JSON.parse(raw);
      state.raw.info = json;
    }

    {
      const raw = await this.request({
        url:
          "http://" +
          this.options.address +
          ":" +
          this.options.port +
          "/players.json"
      });
      const json = JSON.parse(raw);
      state.raw.players = json;
      state.players = [];
      for (const player of json) {
        state.players.push({ name: player.name, ping: player.ping });
      }
      state.raw.version = state.raw.info.version;
      state.raw.numplayers = state.raw.clients;
      state.name = state.name.replace(/\^([0-9])/gm, "");
      state.raw.game = 9000038;
    }
  }
}

module.exports = Gm_fivem;
