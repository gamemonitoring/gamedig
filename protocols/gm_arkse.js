const Valve = require('./valve');

class Gm_arkse extends Valve {
    constructor() {
        super();
    }
    async run(state) {
        await super.run(state);
        let version = state.name.match(/\(v(.*)\)/);
        state.raw.version = version[1];
    }
}

module.exports = Gm_arkse;