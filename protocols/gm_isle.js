const Valve = require('./valve');

class Gm_isle extends Valve {
    constructor() {
        super();
    }
    async run(state) {
        await super.run(state);
        state.raw.version = state.raw.rules.Version_s;
        state.name = state.raw.rules.ServerName_s;
    }
}

module.exports = Gm_isle;