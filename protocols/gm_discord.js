const Core = require("./core");

class Gm_discord extends Core {
  constructor() {
    super();
    this.usedTcp = true;
  }

  async run(state) {
    await super.run(state);

    {
      const url = this.options.connect.split("/");
      const code = url[url.length-1];
      const json = await this.request({
        url: `https://discordapp.com/api/v8/invites/${code}?with_counts=true`
      });
      const raw = JSON.parse(json);

      state.numplayers = raw.approximate_presence_count;
      state.app = 9000041;
      state.request = raw.guild.id;
      state.connect = this.options.connect;
      state.name = raw.guild.name;
      state.maxplayers = raw.approximate_member_count;

      state.raw.gameid = state.app;
      state.raw.numplayers = state.numplayers
    }
  }
}

module.exports = Gm_discord;
