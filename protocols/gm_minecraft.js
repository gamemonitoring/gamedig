const Core = require("./core"),
  MinecraftVanilla = require("./minecraftvanilla"),
  Gamespy3 = require("./gamespy3");

class Gm_minecraft extends Core {
  constructor() {
    super();
    this.srvRecord = "_minecraft._tcp";
  }
  async run(state) {
    const promises = [];

    const vanillaResolver = new MinecraftVanilla();
    vanillaResolver.options = this.options;
    vanillaResolver.udpSocket = this.udpSocket;
    promises.push(
      (async () => {
        try {
          return await vanillaResolver.runOnceSafe();
        } catch (e) {}
      })()
    );

    const bedrockResolver = new Gamespy3();
    bedrockResolver.options = {
      ...this.options,
      encoding: "win1252"
    };
    bedrockResolver.udpSocket = this.udpSocket;
    promises.push(
      (async () => {
        try {
          return await bedrockResolver.runOnceSafe();
        } catch (e) {}
      })()
    );

    const [vanillaState, bedrockState] = await Promise.all(promises);

    if (!vanillaState && !bedrockState) {
      throw new Error("No protocols succeeded");
    }

    if (vanillaState) {
      try {
        let name = "";
        let description = vanillaState.raw.description;
        if (typeof description === "string") {
          name = description;
        }
        if (!name && typeof description === "object" && description.text) {
          name = description.text;
        }
        if (!name && typeof description === "object" && description.extra) {
          name = description.extra.map((part) => part.text).join("");
        }

        state.name = name;
        state.maxplayers = vanillaState.raw.players.max;
        state.raw.numplayers = vanillaState.raw.players.online;
        //state.map = "world";
        state.raw.game = 9000034;
        state.raw.version = vanillaState.raw.version.name;
      } catch (e) {}
      //if (vanillaState.maxplayers) state.maxplayers = vanillaState.maxplayers;
      if (vanillaState.players) state.players = vanillaState.players;
    }
    if (bedrockState) {
      if (bedrockState.name) state.name = bedrockState.name;
      if (bedrockState.maxplayers) state.maxplayers = bedrockState.maxplayers;
      if (bedrockState.players) state.players = bedrockState.players;
      state.raw.game = 123;
    }

    state.name = state.name.replace(/\s+/g, " ");
    state.name = state.name.replace(/\u00A7./g, "");
    state.name = state.name.replace(/&/g, "&amp;");
    state.name = state.name.replace(/'/g, "&#039;");
    state.name = state.name.replace(/</g, "<");
    state.name = state.name.replace(/>/g, ">");
    state.name = state.name.replace(/(�[a-z0-9])/gm, "");

    let version = state.raw.version.match(/([0-9.x]+)/gm);
    version = version[version.length - 1];
    state.raw.version = version;
  }
}

module.exports = Gm_minecraft;
