const Valve = require('./valve');

class Gm_fear_the_night extends Valve {
    constructor() {
        super();
    }
    async run(state) {
        await super.run(state);
        let version = state.name.match(/\(v(.*)\)/);
        state.raw.version = version[1];
    }
}

module.exports = Gm_fear_the_night;