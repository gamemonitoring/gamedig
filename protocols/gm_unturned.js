const Valve = require('./valve');

class Gm_unturned extends Valve {
    constructor() {
        super();
    }
    async run(state) {
        await super.run(state);
        state.raw.version = state.raw.rules.unturned;
    }
}

module.exports = Gm_unturned;