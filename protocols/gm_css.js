const Valve = require("./valve");

class Gm_css extends Valve {
  constructor() {
    super();
  }
  async run(state) {
    await super.run(state);
    state.raw.gameid = 240;
  }
}

module.exports = Gm_css;
